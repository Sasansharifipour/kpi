﻿using DomainClasses;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Services
{
    public interface IDataGatheringService
    {

        List<Gauge_Result_ViewModel> getSDCCH_DR(Operator selected_operator
            , Technology selected_technology);

        List<Gauge_Result_ViewModel> getSDCCH_DR(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime);

        #region 3G

        List<CSSR_Result_ViewModel> getCSSR(Operator selected_operator
            , Technology selected_technology);
        
        List<CSSR_Result_ViewModel> getCSSR(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime);

        List<TCH_ASR_Result_ViewModel> getTCH_ASR(Operator selected_operator
            , Technology selected_technology);

        List<TCH_ASR_Result_ViewModel> getTCH_ASR(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime);


        #endregion

        #region Air

        List<Gauge_Result_ViewModel> get_RAB_Success_Ratio(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime);

        List<Gauge_Result_ViewModel> get_Success_Identity_Response(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime);

        List<Gauge_Result_ViewModel> get_LAU(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime);

        List<Gauge_Result_ViewModel> get_RAU(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime);

        List<Gauge_Result_ViewModel> getRRC_CCSR(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime);

        List<Gauge_Result_ViewModel> getRRC_CSSR(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime);

        List<Gauge_Result_ViewModel> getSuccess_Active_Set_update(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime);

        List<Gauge_Result_ViewModel> getSuccess_Attach_Request(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime);

        List<Gauge_Result_ViewModel> getARSR(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime);

        List<Gauge_Result_ViewModel> getRSRR(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime);

        List<Gauge_Result_ViewModel> getTotal_successful_Call(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime);

        List<Gauge_Result_ViewModel> getSMSSR(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime);

        List<Gauge_Result_ViewModel> get_Success_Authentication_Proceduere_Ratio( 
            Operator selected_operator , Technology selected_technology, 
            DateTime startDateTime, DateTime endDateTime);

        List<Gauge_Result_ViewModel> get_Security_Mode_command_Ratio(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime);

        List<Gauge_Result_ViewModel> get_Soft_HOSR(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime);

        List<Gauge_Result_ViewModel> get_Attachment_Success_Rate(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime);
        #endregion

        #region UMTS

        List<Gauge_Result_ViewModel> get_CS_Call_Drop_Rate(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime);

        List<Gauge_Result_ViewModel> getUMTS_TCH_ASR(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime);

        List<Gauge_Result_ViewModel> getCS_IRAT_HOSR(Operator selected_operator
            , Technology selected_technology);

        List<Gauge_Result_ViewModel> getCS_IRAT_HOSR(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime);

        #endregion

    }

    public class DataGatheringService : IDataGatheringService
    {
        private string generateConnectionString(Operator selected_operator)
        {
            return String.Format("Server={0};Database={1};" +
                                 "User Id={2}; Password={3};",
                selected_operator.IP_Address, selected_operator.Database_Name,
                selected_operator.Username, selected_operator.Password);
        }

        public List<Gauge_Result_ViewModel> getSDCCH_DR(Operator selected_operator, Technology selected_technology)
        {
            List<Gauge_Result_ViewModel> result = new List<Gauge_Result_ViewModel>();

            string connectionString = generateConnectionString(selected_operator);

            string cmd = String.Format("select accurance_date,sum(MM_CMServiceRequest) " +
                                       " as request, sum(MM_CMServiceReject) " +
                                       " as response from (SELECT CONVERT(date, getdate()) " +
                                       " as accurance_date, MM_CMServiceRequest, CASE " +
                                       " WHEN MM_CMServiceReject is not NULL THEN " +
                                       " MM_CMServiceReject " +
                                       " WHEN MM_CMServiceReject is NULL then 0 " +
                                       " END as MM_CMServiceReject" +
                                       " FROM [{0}].[dbo].[{1}]  where MM_CMServiceRequest " +
                                       " is not null and MM_CMServiceRequest > 0) as result " +
                                       " group by accurance_date"
                , selected_operator.Database_Name, selected_technology.Table_Name);

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                if (connection.State == ConnectionState.Open)
                {
                    SqlCommand sqlCommand = new SqlCommand(cmd, connection);

                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                result.Add(
                                    new Gauge_Result_ViewModel()
                                    {
                                        operatorId = selected_operator.Id,

                                        technologyId = selected_technology.Id,

                                        data = ( (double) reader.GetDouble(reader.GetOrdinal("response")) 
                                               / (double) reader.GetDouble(reader.GetOrdinal("request"))),

                                        weight =
                                            reader.GetDouble(reader.GetOrdinal("request")),

                                        accurance_date =
                                            reader.GetDateTime(reader.GetOrdinal("accurance_date"))
                                    }
                                );
                            }
                        }
                    }
                }
            }

            return result;
        }

        public List<Gauge_Result_ViewModel> getSDCCH_DR(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime)
        {
            List<Gauge_Result_ViewModel> result = new List<Gauge_Result_ViewModel>();

            string connectionString = generateConnectionString(selected_operator);

            string cmd = String.Format("select accurance_date,sum(MM_CMServiceRequest) " +
                                       " as request, sum(MM_CMServiceReject) " +
                                       " as response from (SELECT CONVERT(date, DateTime) " +
                                       " as accurance_date, MM_CMServiceRequest, CASE " +
                                       " WHEN MM_CMServiceReject is not NULL THEN " +
                                       " MM_CMServiceReject " +
                                       " WHEN MM_CMServiceReject is NULL then 0 " +
                                       " END as MM_CMServiceReject" +
                                       " FROM [{0}].[dbo].[{1}]  where MM_CMServiceRequest " +
                                       " is not null and MM_CMServiceRequest > 0" +
                                       " and DateTime >= '{2}' and DateTime <= '{3}' ) as result " +
                                       " group by accurance_date"
                , selected_operator.Database_Name, selected_technology.Table_Name
                , startDateTime, endDateTime);

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                if (connection.State == ConnectionState.Open)
                {
                    SqlCommand sqlCommand = new SqlCommand(cmd, connection);

                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                result.Add(
                                    new Gauge_Result_ViewModel()
                                    {
                                        operatorId = selected_operator.Id,

                                        technologyId = selected_technology.Id,

                                        data = ((double)reader.GetDouble(reader.GetOrdinal("response"))
                                               / (double)reader.GetDouble(reader.GetOrdinal("request"))),

                                        weight =
                                            reader.GetDouble(reader.GetOrdinal("request")),

                                        accurance_date =
                                            reader.GetDateTime(reader.GetOrdinal("accurance_date"))
                                    }
                                );
                            }
                        }
                    }
                }
            }

            return result;
        }

        #region 3G

        public List<CSSR_Result_ViewModel> getCSSR(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime)
        {
            List<CSSR_Result_ViewModel> result = new List<CSSR_Result_ViewModel>();

            string connectionString = generateConnectionString(selected_operator);

            string cmd = String.Format("select accurance_date,sum(CC_Setup) " +
                                       " as request, sum(RANAP_RABAssignment_Response) " +
                                       " as response from (SELECT CONVERT(date, DateTime) " +
                                       " as accurance_date, CC_Setup, CASE " +
                                       " WHEN RANAP_RABAssignment_Response is not NULL THEN " +
                                       " RANAP_RABAssignment_Response " +
                                       " WHEN RANAP_RABAssignment_Response is NULL then 0 " +
                                       " END as RANAP_RABAssignment_Response" +
                                       " FROM [{0}].[dbo].[{1}]  where CC_Setup " +
                                       " is not null and CC_Setup > 0" +
                                       " and DateTime >= '{2}' and DateTime <= '{3}' ) as result " +
                                       " group by accurance_date"
                , selected_operator.Database_Name, selected_technology.Table_Name
                , startDateTime, endDateTime);

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                if (connection.State == ConnectionState.Open)
                {
                    SqlCommand sqlCommand = new SqlCommand(cmd, connection);

                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                result.Add(
                                    new CSSR_Result_ViewModel()
                                    {
                                        operatorId = selected_operator.Id,

                                        technologyId = selected_technology.Id,

                                        MM_CMServiceRequest =
                                            reader.GetDouble(reader.GetOrdinal("request")),

                                        RANAP_RABAssignment_Response =
                                            reader.GetDouble(reader.GetOrdinal("response")),

                                        accurance_date =
                                            reader.GetDateTime(reader.GetOrdinal("accurance_date"))
                                    }
                                );
                            }
                        }
                    }
                }
            }

            return result;
        }

        public List<CSSR_Result_ViewModel> getCSSR(Operator selected_operator
            , Technology selected_technology)
        {
            List<CSSR_Result_ViewModel> result = new List<CSSR_Result_ViewModel>();

            string connectionString = generateConnectionString(selected_operator);

            string cmd = String.Format("select accurance_date,sum(MM_CMServiceRequest) " +
                                       " as request, sum(RANAP_RABAssignment_Response) " +
                                       " as response from (SELECT CONVERT(date, getdate()) " +
                                       " as accurance_date, MM_CMServiceRequest, CASE " +
                                       " WHEN RANAP_RABAssignment_Response is not NULL THEN " +
                                       " RANAP_RABAssignment_Response " +
                                       " WHEN RANAP_RABAssignment_Response is NULL then 0 " +
                                       " END as RANAP_RABAssignment_Response" +
                                       " FROM [{0}].[dbo].[{1}]  where MM_CMServiceRequest " +
                                       " is not null and MM_CMServiceRequest > 0) as result " +
                                       " group by accurance_date"
                , selected_operator.Database_Name, selected_technology.Table_Name);

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                if (connection.State == ConnectionState.Open)
                {
                    SqlCommand sqlCommand = new SqlCommand(cmd, connection);

                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                result.Add(
                                    new CSSR_Result_ViewModel()
                                    {
                                        operatorId = selected_operator.Id,

                                        technologyId = selected_technology.Id,

                                        MM_CMServiceRequest =
                                            reader.GetDouble(reader.GetOrdinal("request")),

                                        RANAP_RABAssignment_Response =
                                            reader.GetDouble(reader.GetOrdinal("response")),

                                        accurance_date =
                                            reader.GetDateTime(reader.GetOrdinal("accurance_date"))
                                    }
                                );
                            }
                        }
                    }
                }
            }

            return result;
        }

        public List<TCH_ASR_Result_ViewModel> getTCH_ASR(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime)
        {
            List<TCH_ASR_Result_ViewModel> result = new List<TCH_ASR_Result_ViewModel>();

            string connectionString = generateConnectionString(selected_operator);

            string cmd = String.Format("select accurance_date,sum(RANAP_RABAssignment_Request ) " +
                                       " as request, sum(RANAP_RABAssignment_Response) " +
                                       " as response from (SELECT CONVERT(date, DateTime) " +
                                       " as accurance_date, RANAP_RABAssignment_Request , CASE " +
                                       " WHEN RANAP_RABAssignment_Response is not NULL THEN " +
                                       " RANAP_RABAssignment_Response " +
                                       " WHEN RANAP_RABAssignment_Response is NULL then 0 " +
                                       " END as RANAP_RABAssignment_Response" +
                                       " FROM [{0}].[dbo].[{1}]  where RANAP_RABAssignment_Request  " +
                                       " is not null and RANAP_RABAssignment_Request > 0" +
                                       " and DateTime >= '{2}' and DateTime <= '{3}' ) as result " +
                                       " group by accurance_date"
                , selected_operator.Database_Name, selected_technology.Table_Name
                , startDateTime, endDateTime);

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                if (connection.State == ConnectionState.Open)
                {
                    SqlCommand sqlCommand = new SqlCommand(cmd, connection);

                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                result.Add(
                                    new TCH_ASR_Result_ViewModel()
                                    {
                                        operatorId = selected_operator.Id,

                                        technologyId = selected_technology.Id,

                                        RANAP_RABAssignment_Request =
                                            reader.GetDouble(reader.GetOrdinal("request")),

                                        RANAP_RABAssignment_Response =
                                            reader.GetDouble(reader.GetOrdinal("response")),

                                        accurance_date = reader.GetDateTime
                                            (reader.GetOrdinal("accurance_date"))
                                    }
                                );
                            }
                        }
                    }
                }
            }

            return result;
        }

        public List<TCH_ASR_Result_ViewModel> getTCH_ASR(Operator selected_operator
            , Technology selected_technology)
        {
            List<TCH_ASR_Result_ViewModel> result = new List<TCH_ASR_Result_ViewModel>();

            string connectionString = generateConnectionString(selected_operator);

            string cmd = String.Format("select accurance_date,sum(RANAP_RABAssignment_Request ) " +
                                       " as request, sum(RANAP_RABAssignment_Response) " +
                                       " as response from (SELECT CONVERT(date, getdate()) " +
                                       " as accurance_date, RANAP_RABAssignment_Request , CASE " +
                                       " WHEN RANAP_RABAssignment_Response is not NULL THEN " +
                                       " RANAP_RABAssignment_Response " +
                                       " WHEN RANAP_RABAssignment_Response is NULL then 0 " +
                                       " END as RANAP_RABAssignment_Response" +
                                       " FROM [{0}].[dbo].[{1}]  where RANAP_RABAssignment_Request  " +
                                       " is not null and RANAP_RABAssignment_Request > 0) as result " +
                                       " group by accurance_date"
                , selected_operator.Database_Name, selected_technology.Table_Name);

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                if (connection.State == ConnectionState.Open)
                {
                    SqlCommand sqlCommand = new SqlCommand(cmd, connection);

                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                result.Add(
                                    new TCH_ASR_Result_ViewModel()
                                    {
                                        operatorId = selected_operator.Id,

                                        technologyId = selected_technology.Id,

                                        RANAP_RABAssignment_Request =
                                            reader.GetDouble(reader.GetOrdinal("request")),

                                        RANAP_RABAssignment_Response =
                                            reader.GetDouble(reader.GetOrdinal("response")),

                                        accurance_date = reader.GetDateTime
                                            (reader.GetOrdinal("accurance_date"))
                                    }
                                );
                            }
                        }
                    }
                }
            }

            return result;
        }

        #endregion

        #region Air KPIs Formula

        public List<Gauge_Result_ViewModel> get_Attachment_Success_Rate(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime)
        {
            List<Gauge_Result_ViewModel> result = new List<Gauge_Result_ViewModel>();

            string connectionString = generateConnectionString(selected_operator);

            string cmd = String.Format("select a.accurance_date,a.response,b.request from" +
                                       "(select sum(occurance) as response, accurance_date " +
                                       "from (SELECT[occurance] , CONVERT(date, created_at) " +
                                       " as accurance_date FROM [{0}].[dbo].[analysis_kpi_logs]" +
                                       " where name = N'Attach Accept' " +
                                       "and occurance > 0 and created_at >= '{2}' and " +
                                       " created_at <= '{3}') as a group by accurance_date) as a " +
                                       "FULL JOIN " +
                                       "(select sum(occurance) as request, accurance_date from " +
                                       " (SELECT [occurance]  , CONVERT(date, created_at) " +
                                       "as accurance_date FROM [{0}].[dbo].[analysis_kpi_logs] " +
                                       " where name = N'Attach Request' and " +
                                       " occurance > 0 and created_at >= '{2}' and " +
                                       " created_at <= '{3}') as a group by accurance_date) as b " +
                                       " on a.accurance_date = b.accurance_date"
                , selected_operator.Database_Name, selected_technology.Table_Name
                , startDateTime, endDateTime);

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                if (connection.State == ConnectionState.Open)
                {
                    SqlCommand sqlCommand = new SqlCommand(cmd, connection);

                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                result.Add(
                                    new Gauge_Result_ViewModel()
                                    {
                                        operatorId = selected_operator.Id,

                                        technologyId = selected_technology.Id,

                                        data = ((double)reader.GetDouble(reader.GetOrdinal("response"))
                                                / (double)reader.GetDouble(reader.GetOrdinal("request"))),

                                        weight =
                                            reader.GetDouble(reader.GetOrdinal("request")),

                                        accurance_date =
                                            reader.GetDateTime(reader.GetOrdinal("accurance_date"))
                                    }
                                );
                            }
                        }
                    }
                }
            }

            return result;
        }

        public List<Gauge_Result_ViewModel> get_Soft_HOSR(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime)
        {
            List<Gauge_Result_ViewModel> result = new List<Gauge_Result_ViewModel>();

            string connectionString = generateConnectionString(selected_operator);

            string cmd = String.Format("select a.accurance_date,a.response,b.request from" +
                                       "(select sum(occurance) as response, accurance_date " +
                                       "from (SELECT[occurance] , CONVERT(date, created_at) " +
                                       " as accurance_date FROM [{0}].[dbo].[analysis_kpi_logs]" +
                                       " where name = N'RRC Connection Setup Complete' " +
                                       "and occurance > 0 and created_at >= '{2}' and " +
                                       " created_at <= '{3}') as a group by accurance_date) as a " +
                                       "FULL JOIN " +
                                       "(select sum(occurance) as request, accurance_date from " +
                                       " (SELECT [occurance]  , CONVERT(date, created_at) " +
                                       "as accurance_date FROM [{0}].[dbo].[analysis_kpi_logs] " +
                                       " where name = N'RRC Connection Request' and " +
                                       " occurance > 0 and created_at >= '{2}' and " +
                                       " created_at <= '{3}') as a group by accurance_date) as b " +
                                       " on a.accurance_date = b.accurance_date"
                , selected_operator.Database_Name, selected_technology.Table_Name
                , startDateTime, endDateTime);

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                if (connection.State == ConnectionState.Open)
                {
                    SqlCommand sqlCommand = new SqlCommand(cmd, connection);

                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                result.Add(
                                    new Gauge_Result_ViewModel()
                                    {
                                        operatorId = selected_operator.Id,

                                        technologyId = selected_technology.Id,

                                        data = ((double)reader.GetDouble(reader.GetOrdinal("response"))
                                                / (double)reader.GetDouble(reader.GetOrdinal("request"))),

                                        weight =
                                            reader.GetDouble(reader.GetOrdinal("request")),

                                        accurance_date =
                                            reader.GetDateTime(reader.GetOrdinal("accurance_date"))
                                    }
                                );
                            }
                        }
                    }
                }
            }

            return result;
        }

        public List<Gauge_Result_ViewModel> get_RAB_Success_Ratio(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime)
        {
            List<Gauge_Result_ViewModel> result = new List<Gauge_Result_ViewModel>();

            string connectionString = generateConnectionString(selected_operator);

            string cmd = String.Format("select a.accurance_date,a.response,b.request from" +
                                       "(select sum(occurance) as response, accurance_date " +
                                       "from (SELECT[occurance] , CONVERT(date, created_at) " +
                                       " as accurance_date FROM [{0}].[dbo].[analysis_kpi_logs]" +
                                       " where name = N'Radio Bearer Setup Complete' " +
                                       "and occurance > 0 and created_at >= '{2}' and " +
                                       " created_at <= '{3}') as a group by accurance_date) as a " +
                                       "FULL JOIN " +
                                       "(select sum(occurance) as request, accurance_date from " +
                                       " (SELECT [occurance]  , CONVERT(date, created_at) " +
                                       "as accurance_date FROM [{0}].[dbo].[analysis_kpi_logs] " +
                                       " where name = N'Radio Bearer Release' and " +
                                       " occurance > 0 and created_at >= '{2}' and " +
                                       " created_at <= '{3}') as a group by accurance_date) as b " +
                                       " on a.accurance_date = b.accurance_date"
                , selected_operator.Database_Name, selected_technology.Table_Name
                , startDateTime, endDateTime);

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                if (connection.State == ConnectionState.Open)
                {
                    SqlCommand sqlCommand = new SqlCommand(cmd, connection);

                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                result.Add(
                                    new Gauge_Result_ViewModel()
                                    {
                                        operatorId = selected_operator.Id,

                                        technologyId = selected_technology.Id,

                                        data = ((double)reader.GetDouble(reader.GetOrdinal("response"))
                                                / (double)reader.GetDouble(reader.GetOrdinal("request"))),

                                        weight =
                                            reader.GetDouble(reader.GetOrdinal("request")),

                                        accurance_date =
                                            reader.GetDateTime(reader.GetOrdinal("accurance_date"))
                                    }
                                );
                            }
                        }
                    }
                }
            }

            return result;
        }

        public List<Gauge_Result_ViewModel> get_Success_Identity_Response(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime)
        {
            List<Gauge_Result_ViewModel> result = new List<Gauge_Result_ViewModel>();

            string connectionString = generateConnectionString(selected_operator);

            string cmd = String.Format("select a.accurance_date,a.response,b.request from" +
                                       "(select sum(occurance) as response, accurance_date " +
                                       "from (SELECT[occurance] , CONVERT(date, created_at) " +
                                       " as accurance_date FROM [{0}].[dbo].[analysis_kpi_logs]" +
                                       " where name = N'Identity Response' " +
                                       "and occurance > 0 and created_at >= '{2}' and " +
                                       " created_at <= '{3}') as a group by accurance_date) as a " +
                                       "FULL JOIN " +
                                       "(select sum(occurance) as request, accurance_date from " +
                                       " (SELECT [occurance]  , CONVERT(date, created_at) " +
                                       "as accurance_date FROM [{0}].[dbo].[analysis_kpi_logs] " +
                                       " where name = N'Identity Request' and " +
                                       " occurance > 0 and created_at >= '{2}' and " +
                                       " created_at <= '{3}') as a group by accurance_date) as b " +
                                       " on a.accurance_date = b.accurance_date"
                , selected_operator.Database_Name, selected_technology.Table_Name
                , startDateTime, endDateTime);

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                if (connection.State == ConnectionState.Open)
                {
                    SqlCommand sqlCommand = new SqlCommand(cmd, connection);

                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                result.Add(
                                    new Gauge_Result_ViewModel()
                                    {
                                        operatorId = selected_operator.Id,

                                        technologyId = selected_technology.Id,

                                        data = ((double)reader.GetDouble(reader.GetOrdinal("response"))
                                                / (double)reader.GetDouble(reader.GetOrdinal("request"))),

                                        weight =
                                            reader.GetDouble(reader.GetOrdinal("request")),

                                        accurance_date =
                                            reader.GetDateTime(reader.GetOrdinal("accurance_date"))
                                    }
                                );
                            }
                        }
                    }
                }
            }

            return result;
        }

        public List<Gauge_Result_ViewModel> get_LAU(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime)
        {
            List<Gauge_Result_ViewModel> result = new List<Gauge_Result_ViewModel>();

            string connectionString = generateConnectionString(selected_operator);

            string cmd = String.Format("select a.accurance_date,a.response,b.request from" +
                                       "(select sum(occurance) as response, accurance_date " +
                                       "from (SELECT[occurance] , CONVERT(date, created_at) " +
                                       " as accurance_date FROM [{0}].[dbo].[analysis_kpi_logs]" +
                                       " where name = N'Location Area Update Accept' " +
                                       "and occurance > 0 and created_at >= '{2}' and " +
                                       " created_at <= '{3}') as a group by accurance_date) as a " +
                                       "FULL JOIN " +
                                       "(select sum(occurance) as request, accurance_date from " +
                                       " (SELECT [occurance]  , CONVERT(date, created_at) " +
                                       "as accurance_date FROM [{0}].[dbo].[analysis_kpi_logs] " +
                                       " where name = N'Location Area Update Request' and " +
                                       " occurance > 0 and created_at >= '{2}' and " +
                                       " created_at <= '{3}') as a group by accurance_date) as b " +
                                       " on a.accurance_date = b.accurance_date"
                , selected_operator.Database_Name, selected_technology.Table_Name
                , startDateTime, endDateTime);

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                if (connection.State == ConnectionState.Open)
                {
                    SqlCommand sqlCommand = new SqlCommand(cmd, connection);

                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                result.Add(
                                    new Gauge_Result_ViewModel()
                                    {
                                        operatorId = selected_operator.Id,

                                        technologyId = selected_technology.Id,

                                        data = ((double)reader.GetDouble(reader.GetOrdinal("response"))
                                                / (double)reader.GetDouble(reader.GetOrdinal("request"))),

                                        weight =
                                            reader.GetDouble(reader.GetOrdinal("request")),

                                        accurance_date =
                                            reader.GetDateTime(reader.GetOrdinal("accurance_date"))
                                    }
                                );
                            }
                        }
                    }
                }
            }

            return result;
        }

        public List<Gauge_Result_ViewModel> get_RAU(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime)
        {
            List<Gauge_Result_ViewModel> result = new List<Gauge_Result_ViewModel>();

            string connectionString = generateConnectionString(selected_operator);

            string cmd = String.Format("select a.accurance_date,a.response,b.request from" +
                                       "(select sum(occurance) as response, accurance_date " +
                                       "from (SELECT[occurance] , CONVERT(date, created_at) " +
                                       " as accurance_date FROM [{0}].[dbo].[analysis_kpi_logs]" +
                                       " where name = N'Routing Update Accept' " +
                                       "and occurance > 0 and created_at >= '{2}' and " +
                                       " created_at <= '{3}') as a group by accurance_date) as a " +
                                       "FULL JOIN " +
                                       "(select sum(occurance) as request, accurance_date from " +
                                       " (SELECT [occurance]  , CONVERT(date, created_at) " +
                                       "as accurance_date FROM [{0}].[dbo].[analysis_kpi_logs] " +
                                       " where name = N'Routing Update Request' and " +
                                       " occurance > 0 and created_at >= '{2}' and " +
                                       " created_at <= '{3}') as a group by accurance_date) as b " +
                                       " on a.accurance_date = b.accurance_date"
                , selected_operator.Database_Name, selected_technology.Table_Name
                , startDateTime, endDateTime);

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                if (connection.State == ConnectionState.Open)
                {
                    SqlCommand sqlCommand = new SqlCommand(cmd, connection);

                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                result.Add(
                                    new Gauge_Result_ViewModel()
                                    {
                                        operatorId = selected_operator.Id,

                                        technologyId = selected_technology.Id,

                                        data = ((double)reader.GetDouble(reader.GetOrdinal("response"))
                                                / (double)reader.GetDouble(reader.GetOrdinal("request"))),

                                        weight =
                                            reader.GetDouble(reader.GetOrdinal("request")),

                                        accurance_date =
                                            reader.GetDateTime(reader.GetOrdinal("accurance_date"))
                                    }
                                );
                            }
                        }
                    }
                }
            }

            return result;
        }
        
        public List<Gauge_Result_ViewModel> getRRC_CCSR(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime)
        {
            List<Gauge_Result_ViewModel> result = new List<Gauge_Result_ViewModel>();

            string connectionString = generateConnectionString(selected_operator);

            string cmd = String.Format("select a.accurance_date,a.response,b.request from" +
                                       "(select sum(occurance) as response, accurance_date " +
                                       "from (SELECT[occurance] , CONVERT(date, created_at) " +
                                       " as accurance_date FROM [{0}].[dbo].[analysis_kpi_logs]" +
                                       " where name = N'RRC Connection Setup Complete' " +
                                       "and occurance > 0 and created_at >= '{2}' and " +
                                       " created_at <= '{3}') as a group by accurance_date) as a " +
                                       "FULL JOIN " +
                                       "(select sum(occurance) as request, accurance_date from " +
                                       " (SELECT [occurance]  , CONVERT(date, created_at) " +
                                       "as accurance_date FROM [{0}].[dbo].[analysis_kpi_logs] " +
                                       " where name = N'RRC Connection Request' and " +
                                       " occurance > 0 and created_at >= '{2}' and " +
                                       " created_at <= '{3}') as a group by accurance_date) as b " +
                                       " on a.accurance_date = b.accurance_date"
                , selected_operator.Database_Name, selected_technology.Table_Name
                , startDateTime, endDateTime);

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                if (connection.State == ConnectionState.Open)
                {
                    SqlCommand sqlCommand = new SqlCommand(cmd, connection);

                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                result.Add(
                                    new Gauge_Result_ViewModel()
                                    {
                                        operatorId = selected_operator.Id,

                                        technologyId = selected_technology.Id,

                                        data = ((double)reader.GetDouble(reader.GetOrdinal("response"))
                                                / (double)reader.GetDouble(reader.GetOrdinal("request"))),

                                        weight =
                                            reader.GetDouble(reader.GetOrdinal("request")),

                                        accurance_date =
                                            reader.GetDateTime(reader.GetOrdinal("accurance_date"))
                                    }
                                );
                            }
                        }
                    }
                }
            }

            return result;
        }

        public List<Gauge_Result_ViewModel> getRRC_CSSR(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime)
        {
            List<Gauge_Result_ViewModel> result = new List<Gauge_Result_ViewModel>();

            string connectionString = generateConnectionString(selected_operator);

            string cmd = String.Format("select a.accurance_date,a.response,b.request from" +
                                       "(select sum(occurance) as response, accurance_date " +
                                       "from (SELECT[occurance] , CONVERT(date, created_at) " +
                                       " as accurance_date FROM [{0}].[dbo].[analysis_kpi_logs]" +
                                       " where name = N'RRC Connection Setup Complete' " +
                                       "and occurance > 0 and created_at >= '{2}' and " +
                                       " created_at <= '{3}') as a group by accurance_date) as a " +
                                       "FULL JOIN " +
                                       "(select sum(occurance) as request, accurance_date from " +
                                       " (SELECT [occurance]  , CONVERT(date, created_at) " +
                                       "as accurance_date FROM [{0}].[dbo].[analysis_kpi_logs] " +
                                       " where name = N'RRC Connection Setup' and " +
                                       " occurance > 0 and created_at >= '{2}' and " +
                                       " created_at <= '{3}') as a group by accurance_date) as b " +
                                       " on a.accurance_date = b.accurance_date"
                , selected_operator.Database_Name, selected_technology.Table_Name
                , startDateTime, endDateTime);

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                if (connection.State == ConnectionState.Open)
                {
                    SqlCommand sqlCommand = new SqlCommand(cmd, connection);

                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                result.Add(
                                    new Gauge_Result_ViewModel()
                                    {
                                        operatorId = selected_operator.Id,

                                        technologyId = selected_technology.Id,

                                        data = ((double)reader.GetDouble(reader.GetOrdinal("response"))
                                                / (double)reader.GetDouble(reader.GetOrdinal("request"))),

                                        weight =
                                            reader.GetDouble(reader.GetOrdinal("request")),

                                        accurance_date =
                                            reader.GetDateTime(reader.GetOrdinal("accurance_date"))
                                    }
                                );
                            }
                        }
                    }
                }
            }

            return result;
        }

        public List<Gauge_Result_ViewModel> getSuccess_Active_Set_update(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime)
        {
            List<Gauge_Result_ViewModel> result = new List<Gauge_Result_ViewModel>();

            string connectionString = generateConnectionString(selected_operator);

            string cmd = String.Format("select a.accurance_date,a.response,b.request from" +
                                       "(select sum(occurance) as response, accurance_date " +
                                       "from (SELECT[occurance] , CONVERT(date, created_at) " +
                                       " as accurance_date FROM [{0}].[dbo].[analysis_kpi_logs]" +
                                       " where name = N'Active Set Update Complete' " +
                                       "and occurance > 0 and created_at >= '{2}' and " +
                                       " created_at <= '{3}') as a group by accurance_date) as a " +
                                       "FULL JOIN " +
                                       "(select sum(occurance) as request, accurance_date from " +
                                       " (SELECT [occurance]  , CONVERT(date, created_at) " +
                                       "as accurance_date FROM [{0}].[dbo].[analysis_kpi_logs] " +
                                       " where name = N'Active Set Update' and " +
                                       " occurance > 0 and created_at >= '{2}' and " +
                                       " created_at <= '{3}') as a group by accurance_date) as b " +
                                       " on a.accurance_date = b.accurance_date"
                , selected_operator.Database_Name, selected_technology.Table_Name
                , startDateTime, endDateTime);

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                if (connection.State == ConnectionState.Open)
                {
                    SqlCommand sqlCommand = new SqlCommand(cmd, connection);

                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                result.Add(
                                    new Gauge_Result_ViewModel()
                                    {
                                        operatorId = selected_operator.Id,

                                        technologyId = selected_technology.Id,

                                        data = ((double)reader.GetDouble(reader.GetOrdinal("response"))
                                                / (double)reader.GetDouble(reader.GetOrdinal("request"))),

                                        weight =
                                            reader.GetDouble(reader.GetOrdinal("request")),

                                        accurance_date =
                                            reader.GetDateTime(reader.GetOrdinal("accurance_date"))
                                    }
                                );
                            }
                        }
                    }
                }
            }

            return result;
        }

        public List<Gauge_Result_ViewModel> getSuccess_Attach_Request(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime)
        {
            List<Gauge_Result_ViewModel> result = new List<Gauge_Result_ViewModel>();

            string connectionString = generateConnectionString(selected_operator);

            string cmd = String.Format("select a.accurance_date,a.response,b.request from" +
                                       "(select sum(occurance) as response, accurance_date " +
                                       "from (SELECT[occurance] , CONVERT(date, created_at) " +
                                       " as accurance_date FROM [{0}].[dbo].[analysis_kpi_logs]" +
                                       " where name = N'Attach_Accept' " +
                                       "and occurance > 0 and created_at >= '{2}' and " +
                                       " created_at <= '{3}') as a group by accurance_date) as a " +
                                       "FULL JOIN " +
                                       "(select sum(occurance) as request, accurance_date from " +
                                       " (SELECT [occurance]  , CONVERT(date, created_at) " +
                                       "as accurance_date FROM [{0}].[dbo].[analysis_kpi_logs] " +
                                       " where name = N'Attach_Request' and " +
                                       " occurance > 0 and created_at >= '{2}' and " +
                                       " created_at <= '{3}') as a group by accurance_date) as b " +
                                       " on a.accurance_date = b.accurance_date"
                , selected_operator.Database_Name, selected_technology.Table_Name
                , startDateTime, endDateTime);

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                if (connection.State == ConnectionState.Open)
                {
                    SqlCommand sqlCommand = new SqlCommand(cmd, connection);

                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                result.Add(
                                    new Gauge_Result_ViewModel()
                                    {
                                        operatorId = selected_operator.Id,

                                        technologyId = selected_technology.Id,

                                        data = ((double)reader.GetDouble(reader.GetOrdinal("response"))
                                                / (double)reader.GetDouble(reader.GetOrdinal("request"))),

                                        weight =
                                            reader.GetDouble(reader.GetOrdinal("request")),

                                        accurance_date =
                                            reader.GetDateTime(reader.GetOrdinal("accurance_date"))
                                    }
                                );
                            }
                        }
                    }
                }
            }

            return result;
        }

        public List<Gauge_Result_ViewModel> getARSR(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime)
        {
            List<Gauge_Result_ViewModel> result = new List<Gauge_Result_ViewModel>();

            string connectionString = generateConnectionString(selected_operator);

            string cmd = String.Format("select a.accurance_date,a.response,b.request from" +
                                       "(select sum(occurance) as response, accurance_date " +
                                       "from (SELECT[occurance] , CONVERT(date, created_at) " +
                                       " as accurance_date FROM [{0}].[dbo].[analysis_kpi_logs]" +
                                       " where name = N'Radio Bearer Setup Complete' " +
                                       "and occurance > 0 and created_at >= '{2}' and " +
                                       " created_at <= '{3}') as a group by accurance_date) as a " +
                                       "FULL JOIN " +
                                       "(select sum(occurance) as request, accurance_date from " +
                                       " (SELECT [occurance]  , CONVERT(date, created_at) " +
                                       "as accurance_date FROM [{0}].[dbo].[analysis_kpi_logs] " +
                                       " where name = N'Radio Bearer Setup' and " +
                                       " occurance > 0 and created_at >= '{2}' and " +
                                       " created_at <= '{3}') as a group by accurance_date) as b " +
                                       " on a.accurance_date = b.accurance_date"
                , selected_operator.Database_Name, selected_technology.Table_Name
                , startDateTime, endDateTime);

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                if (connection.State == ConnectionState.Open)
                {
                    SqlCommand sqlCommand = new SqlCommand(cmd, connection);

                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                result.Add(
                                    new Gauge_Result_ViewModel()
                                    {
                                        operatorId = selected_operator.Id,

                                        technologyId = selected_technology.Id,

                                        data = ((double)reader.GetDouble(reader.GetOrdinal("response"))
                                                / (double)reader.GetDouble(reader.GetOrdinal("request"))),

                                        weight =
                                            reader.GetDouble(reader.GetOrdinal("request")),

                                        accurance_date =
                                            reader.GetDateTime(reader.GetOrdinal("accurance_date"))
                                    }
                                );
                            }
                        }
                    }
                }
            }

            return result;
        }

        public List<Gauge_Result_ViewModel> getRSRR(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime)
        {
            List<Gauge_Result_ViewModel> result = new List<Gauge_Result_ViewModel>();

            string connectionString = generateConnectionString(selected_operator);

            string cmd = String.Format(" select a.response,b.request,coalesce(a.accurance_date,b.accurance_date)" +
                                       " as accurance_date from(select " +
                                       " coalesce(a.response + b.response, a.response, b.response, 0) as response," +
                                       " coalesce(a.accurance_date, b.accurance_date) as accurance_date" +
                                       " from (select coalesce(sum(occurance), 0) as response, accurance_date " +
                                       " from(SELECT[occurance], CONVERT(date, created_at) " +
                                       " as accurance_date FROM[{0}].[dbo].[analysis_kpi_logs]" +
                                       " where name = N'Radio Bearer Setup Complete' " +
                                       " and occurance > 0 and created_at >= '{2}' and " +
                                       " created_at <= '{3}') as a group by accurance_date) as a" +
                                       " FULL JOIN (select coalesce(sum(occurance), 0) as response, accurance_date" +
                                       " from(SELECT[occurance], CONVERT(date, created_at)" +
                                       " as accurance_date FROM[{0}].[dbo].[analysis_kpi_logs] " +
                                       " where name = N'Release Complete'" +
                                       " and occurance > 0 and created_at >= '{2}' and " +
                                       " created_at <= '{3}') as a group by accurance_date) as b" +
                                       " on a.accurance_date = b.accurance_date) as a" +
                                       " FULL JOIN ( select " +
                                       " coalesce(a.request + b.request, a.request, b.request, 0) as request," +
                                       " coalesce(a.accurance_date, b.accurance_date) as accurance_date " +
                                       " from (select coalesce(sum(occurance), 0) as request, accurance_date " +
                                       " from(SELECT[occurance], CONVERT(date, created_at) " +
                                       " as accurance_date FROM[{0}].[dbo].[analysis_kpi_logs]" +
                                       " where name = N'Radio Bearer Release'" +
                                       " and occurance > 0 and created_at >= '{2}' and " +
                                       " created_at <= '{3}') as a group by accurance_date) as a" +
                                       " FULL JOIN (select coalesce(sum(occurance), 0) as request, accurance_date " +
                                       " from(SELECT[occurance], CONVERT(date, created_at) " +
                                       " as accurance_date FROM[{0}].[dbo].[analysis_kpi_logs] " +
                                       " where name = N'Release' " +
                                       " and occurance > 0 and created_at >= '{2}' and " +
                                       " created_at <= '{3}') as a group by accurance_date) as b " +
                                       " on a.accurance_date = b.accurance_date ) as b" +
                                       " on a.accurance_date = b.accurance_date"
                , selected_operator.Database_Name, selected_technology.Table_Name
                , startDateTime, endDateTime);

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                if (connection.State == ConnectionState.Open)
                {
                    SqlCommand sqlCommand = new SqlCommand(cmd, connection);

                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                result.Add(
                                    new Gauge_Result_ViewModel()
                                    {
                                        operatorId = selected_operator.Id,

                                        technologyId = selected_technology.Id,

                                        data = ((double)reader.GetDouble(reader.GetOrdinal("response"))
                                                / (double)reader.GetDouble(reader.GetOrdinal("request"))),

                                        weight =
                                            reader.GetDouble(reader.GetOrdinal("request")),

                                        accurance_date =
                                            reader.GetDateTime(reader.GetOrdinal("accurance_date"))
                                    }
                                );
                            }
                        }
                    }
                }
            }

            return result;
        }

        public List<Gauge_Result_ViewModel> getTotal_successful_Call(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime)
        {
            List<Gauge_Result_ViewModel> result = new List<Gauge_Result_ViewModel>();

            string connectionString = generateConnectionString(selected_operator);

            string cmd = String.Format(" select a.response,b.request,coalesce(a.accurance_date,b.accurance_date)" +
                                       " as accurance_date from(select " +
                                       " coalesce(a.response + b.response, a.response, b.response, 0) as response," +
                                       " coalesce(a.accurance_date, b.accurance_date) as accurance_date" +
                                       " from (select coalesce(sum(occurance), 0) as response, accurance_date " +
                                       " from(SELECT[occurance], CONVERT(date, created_at) " +
                                       " as accurance_date FROM[{0}].[dbo].[analysis_kpi_logs]" +
                                       " where name = N'Call Confirmed' " +
                                       " and occurance > 0 and created_at >= '{2}' and " +
                                       " created_at <= '{3}') as a group by accurance_date) as a" +
                                       " FULL JOIN (select coalesce(sum(occurance), 0 ) as response, accurance_date" +
                                       " from(SELECT[occurance], CONVERT(date, created_at)" +
                                       " as accurance_date FROM[{0}].[dbo].[analysis_kpi_logs]" +
                                       " where name = N'Call Proceeding'" +
                                       " and occurance > 0 and created_at >= '{2}' and " +
                                       " created_at <= '{3}') as a group by accurance_date) as b" +
                                       " on a.accurance_date = b.accurance_date) as a " +
                                       " FULL JOIN (select sum(occurance) as request, accurance_date from" +
                                       " (SELECT[occurance], CONVERT(date, created_at) " +
                                       " as accurance_date FROM[{0}].[dbo].[analysis_kpi_logs] " +
                                       " where name = N'Setup' and" +
                                       " occurance > 0 and created_at >= '{2}' and " +
                                       " created_at <= '{3}') as a group by accurance_date) as b" +
                                       " on a.accurance_date = b.accurance_date"
                , selected_operator.Database_Name, selected_technology.Table_Name
                , startDateTime, endDateTime);

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                if (connection.State == ConnectionState.Open)
                {
                    SqlCommand sqlCommand = new SqlCommand(cmd, connection);

                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                result.Add(
                                    new Gauge_Result_ViewModel()
                                    {
                                        operatorId = selected_operator.Id,

                                        technologyId = selected_technology.Id,

                                        data = ((double)reader.GetDouble(reader.GetOrdinal("response"))
                                                / (double)reader.GetDouble(reader.GetOrdinal("request"))),

                                        weight =
                                            reader.GetDouble(reader.GetOrdinal("request")),

                                        accurance_date =
                                            reader.GetDateTime(reader.GetOrdinal("accurance_date"))
                                    }
                                );
                            }
                        }
                    }
                }
            }

            return result;
        }

        public List<Gauge_Result_ViewModel> getSMSSR(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime)
        {
            List<Gauge_Result_ViewModel> result = new List<Gauge_Result_ViewModel>();

            string connectionString = generateConnectionString(selected_operator);

            string cmd = String.Format("select a.accurance_date,a.response,b.request from" +
                                       "(select sum(occurance) as response, accurance_date " +
                                       "from (SELECT[occurance] , CONVERT(date, created_at) " +
                                       " as accurance_date FROM [{0}].[dbo].[analysis_kpi_logs]" +
                                       " where name = N'CP ACK' " +
                                       "and occurance > 0 and created_at >= '{2}' and " +
                                       " created_at <= '{3}') as a group by accurance_date) as a " +
                                       "FULL JOIN " +
                                       "(select sum(occurance) as request, accurance_date from " +
                                       " (SELECT [occurance]  , CONVERT(date, created_at) " +
                                       "as accurance_date FROM [{0}].[dbo].[analysis_kpi_logs] " +
                                       " where name = N'CP DATA' and " +
                                       " occurance > 0 and created_at >= '{2}' and " +
                                       " created_at <= '{3}') as a group by accurance_date) as b " +
                                       " on a.accurance_date = b.accurance_date"
                , selected_operator.Database_Name, selected_technology.Table_Name
                , startDateTime, endDateTime);

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                if (connection.State == ConnectionState.Open)
                {
                    SqlCommand sqlCommand = new SqlCommand(cmd, connection);

                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                result.Add(
                                    new Gauge_Result_ViewModel()
                                    {
                                        operatorId = selected_operator.Id,

                                        technologyId = selected_technology.Id,

                                        data = ((double)reader.GetDouble(reader.GetOrdinal("response"))
                                                / (double)reader.GetDouble(reader.GetOrdinal("request"))),

                                        weight =
                                            reader.GetDouble(reader.GetOrdinal("request")),

                                        accurance_date =
                                            reader.GetDateTime(reader.GetOrdinal("accurance_date"))
                                    }
                                );
                            }
                        }
                    }
                }
            }

            return result;
        }

        public List<Gauge_Result_ViewModel> get_Success_Authentication_Proceduere_Ratio(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime)
        {
            List<Gauge_Result_ViewModel> result = new List<Gauge_Result_ViewModel>();

            string connectionString = generateConnectionString(selected_operator);

            string cmd = String.Format("select a.accurance_date,a.response,b.request from" +
                                       "(select sum(occurance) as response, accurance_date " +
                                       "from (SELECT[occurance] , CONVERT(date, created_at) " +
                                       " as accurance_date FROM [{0}].[dbo].[analysis_kpi_logs]" +
                                       " where name = N'Authentication Response' " +
                                       "and occurance > 0 and created_at >= '{2}' and " +
                                       " created_at <= '{3}') as a group by accurance_date) as a " +
                                       "FULL JOIN " +
                                       "(select sum(occurance) as request, accurance_date from " +
                                       " (SELECT [occurance]  , CONVERT(date, created_at) " +
                                       "as accurance_date FROM [{0}].[dbo].[analysis_kpi_logs] " +
                                       " where name = N'Authentication Request' and " +
                                       " occurance > 0 and created_at >= '{2}' and " +
                                       " created_at <= '{3}') as a group by accurance_date) as b " +
                                       " on a.accurance_date = b.accurance_date"
                , selected_operator.Database_Name, selected_technology.Table_Name
                , startDateTime, endDateTime);

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                if (connection.State == ConnectionState.Open)
                {
                    SqlCommand sqlCommand = new SqlCommand(cmd, connection);

                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                result.Add(
                                    new Gauge_Result_ViewModel()
                                    {
                                        operatorId = selected_operator.Id,

                                        technologyId = selected_technology.Id,

                                        data = ((double)reader.GetDouble(reader.GetOrdinal("response"))
                                                / (double)reader.GetDouble(reader.GetOrdinal("request"))),

                                        weight =
                                            reader.GetDouble(reader.GetOrdinal("request")),

                                        accurance_date =
                                            reader.GetDateTime(reader.GetOrdinal("accurance_date"))
                                    }
                                );
                            }
                        }
                    }
                }
            }

            return result;
        }

        public List<Gauge_Result_ViewModel> get_Security_Mode_command_Ratio(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime)
        {
            List<Gauge_Result_ViewModel> result = new List<Gauge_Result_ViewModel>();

            string connectionString = generateConnectionString(selected_operator);

            string cmd = String.Format("select a.accurance_date,a.response,b.request from" +
                                       "(select sum(occurance) as response, accurance_date " +
                                       "from (SELECT[occurance] , CONVERT(date, created_at) " +
                                       " as accurance_date FROM [{0}].[dbo].[analysis_kpi_logs]" +
                                       " where name = N'Security Mode Complete' " +
                                       "and occurance > 0 and created_at >= '{2}' and " +
                                       " created_at <= '{3}') as a group by accurance_date) as a " +
                                       "FULL JOIN " +
                                       "(select sum(occurance) as request, accurance_date from " +
                                       " (SELECT [occurance]  , CONVERT(date, created_at) " +
                                       "as accurance_date FROM [{0}].[dbo].[analysis_kpi_logs] " +
                                       " where name = N'Security Mode Command' and " +
                                       " occurance > 0 and created_at >= '{2}' and " +
                                       " created_at <= '{3}') as a group by accurance_date) as b " +
                                       " on a.accurance_date = b.accurance_date"
                , selected_operator.Database_Name, selected_technology.Table_Name
                , startDateTime, endDateTime);

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                if (connection.State == ConnectionState.Open)
                {
                    SqlCommand sqlCommand = new SqlCommand(cmd, connection);

                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                result.Add(
                                    new Gauge_Result_ViewModel()
                                    {
                                        operatorId = selected_operator.Id,

                                        technologyId = selected_technology.Id,

                                        data = ((double)reader.GetDouble(reader.GetOrdinal("response"))
                                                / (double)reader.GetDouble(reader.GetOrdinal("request"))),

                                        weight =
                                            reader.GetDouble(reader.GetOrdinal("request")),

                                        accurance_date =
                                            reader.GetDateTime(reader.GetOrdinal("accurance_date"))
                                    }
                                );
                            }
                        }
                    }
                }
            }

            return result;
        }

        #endregion

        #region UMTS KPIs Formula

        public List<Gauge_Result_ViewModel> get_CS_Call_Drop_Rate(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime)
        {
            List<Gauge_Result_ViewModel> result = new List<Gauge_Result_ViewModel>();

            string connectionString = generateConnectionString(selected_operator);
            
            string cmd = String.Format("SELECT accurance_date  ,sum(Unexpected_Drop) as response " +
                                       " ,sum(RANAP_RABAssignment_Response) as request " +
                                       " from (Select CONVERT(date, DateTime) as accurance_date, "+
                                       " b.Unexpected_Drop, a.RANAP_RABAssignment_Response " +
                                       " from [{0}].[dbo].[{1}] as a "+
                                       " inner join(SELECT [ID] " +
                                       " ,[Cause_1] + [Cause_2] + [Cause_3] + [Cause_4] +[Cause_5] +[Cause_6] +[Cause_7] +[Cause_8] +[Cause_9] +[Cause_10] "+
                                       " +[Cause_11] +[Cause_12] +[Cause_13] +[Cause_14] +[Cause_15] +[Cause_16] +[Cause_17] +[Cause_18] +[Cause_19] +[Cause_20] "+
                                       " +[Cause_21] +[Cause_22] +[Cause_23] +[Cause_24] +[Cause_25] +[Cause_26] +[Cause_27] +[Cause_28] +[Cause_29] +[Cause_30] "+
                                       " +[Cause_31] +[Cause_32] +[Cause_33] +[Cause_34] +[Cause_35] +[Cause_36] +[Cause_37] +[Cause_38] +[Cause_39] +[Cause_40] "+
                                       " +[Cause_41] +[Cause_42] +[Cause_43] +[Cause_44] +[Cause_45] +[Cause_46] +[Cause_47] +[Cause_48] +[Cause_49] +[Cause_50] "+
                                       " +[Cause_51] +[Cause_52] +[Cause_53] +[Cause_54] +[Cause_55] +[Cause_56] +[Cause_57] +[Cause_58] +[Cause_59] +[Cause_60] "+
                                       " +[Cause_61] +[Cause_62] +[Cause_63] +[Cause_64] +[Cause_65] +[Cause_66] +[Cause_81] +[Cause_82] +[Cause_83] "+
                                       " +[Cause_84] +[Cause_97] +[Cause_98] +[Cause_99] +[Cause_100] +[Cause_101] +[Cause_102] +[Cause_113] +[Cause_114] +[Cause_115] "+
                                       " +[Cause_116] +[Cause_257] +[Cause_258] +[Cause_259] +[Cause_260] +[Cause_261] +[Cause_262] +[Cause_263] +[Cause_264] +[Cause_265] " +
                                       " +[Cause_266] +[Cause_267] +[Cause_268] - [Cause_83] as Unexpected_Drop "+
                                       " FROM [{0}].[dbo].[{2}]) as b "+
                                       " on a.ID = b.ID where a.RANAP_RABAssignment_Response > 0" +
                                       " and DateTime >= '{3}' and DateTime <= '{4}') as result " +
                                       " group by accurance_date"
                , selected_operator.Database_Name, selected_technology.Table_Name
                , selected_technology.Cause_Table_Name , startDateTime, endDateTime);

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                if (connection.State == ConnectionState.Open)
                {
                    SqlCommand sqlCommand = new SqlCommand(cmd, connection);

                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                result.Add(
                                    new Gauge_Result_ViewModel()
                                    {
                                        operatorId = selected_operator.Id,

                                        technologyId = selected_technology.Id,

                                        data = ((double)reader.GetDouble(reader.GetOrdinal("response"))
                                                / (double)reader.GetDouble(reader.GetOrdinal("request"))),

                                        weight =
                                            reader.GetDouble(reader.GetOrdinal("request")),

                                        accurance_date =
                                            reader.GetDateTime(reader.GetOrdinal("accurance_date"))
                                    }
                                );
                            }
                        }
                    }
                }
            }

            return result;
        }

        public List<Gauge_Result_ViewModel> getCS_IRAT_HOSR(Operator selected_operator, Technology selected_technology)
        {
            List<Gauge_Result_ViewModel> result = new List<Gauge_Result_ViewModel>();

            string connectionString = generateConnectionString(selected_operator);

            string cmd = String.Format("select accurance_date,sum(MM_TMSIReallocationCommand) " +
                                       " as request, sum(MM_TMSIReallocationComplete) " +
                                       " as response from (SELECT CONVERT(date, getdate()) " +
                                       " as accurance_date, MM_TMSIReallocationCommand, CASE " +
                                       " WHEN MM_TMSIReallocationComplete is not NULL THEN " +
                                       " MM_TMSIReallocationComplete " +
                                       " WHEN MM_TMSIReallocationComplete is NULL then 0 " +
                                       " END as MM_TMSIReallocationComplete" +
                                       " FROM [{0}].[dbo].[{1}]  where MM_TMSIReallocationCommand " +
                                       " is not null and MM_TMSIReallocationCommand > 0) as result " +
                                       " group by accurance_date"
                , selected_operator.Database_Name, selected_technology.Table_Name);

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                if (connection.State == ConnectionState.Open)
                {
                    SqlCommand sqlCommand = new SqlCommand(cmd, connection);

                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                result.Add(
                                    new Gauge_Result_ViewModel()
                                    {
                                        operatorId = selected_operator.Id,

                                        technologyId = selected_technology.Id,

                                        data = ((double)reader.GetDouble(reader.GetOrdinal("response"))
                                               / (double)reader.GetDouble(reader.GetOrdinal("request"))),

                                        weight =
                                            reader.GetDouble(reader.GetOrdinal("request")),

                                        accurance_date =
                                            reader.GetDateTime(reader.GetOrdinal("accurance_date"))
                                    }
                                );
                            }
                        }
                    }
                }
            }

            return result;
        }

        public List<Gauge_Result_ViewModel> getCS_IRAT_HOSR(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime)
        {
            List<Gauge_Result_ViewModel> result = new List<Gauge_Result_ViewModel>();

            string connectionString = generateConnectionString(selected_operator);

            string cmd = String.Format("select accurance_date,sum(MM_TMSIReallocationCommand) " +
                                       " as request, sum(MM_TMSIReallocationComplete) " +
                                       " as response from (SELECT CONVERT(date, DateTime) " +
                                       " as accurance_date, MM_TMSIReallocationCommand, CASE " +
                                       " WHEN MM_TMSIReallocationComplete is not NULL THEN " +
                                       " MM_TMSIReallocationComplete " +
                                       " WHEN MM_TMSIReallocationComplete is NULL then 0 " +
                                       " END as MM_TMSIReallocationComplete" +
                                       " FROM [{0}].[dbo].[{1}]  where MM_TMSIReallocationCommand " +
                                       " is not null and MM_TMSIReallocationCommand > 0" +
                                       " and DateTime >= '{2}' and DateTime <= '{3}' ) as result " +
                                       " group by accurance_date"
                , selected_operator.Database_Name, selected_technology.Table_Name
                , startDateTime, endDateTime);

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                if (connection.State == ConnectionState.Open)
                {
                    SqlCommand sqlCommand = new SqlCommand(cmd, connection);

                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                result.Add(
                                    new Gauge_Result_ViewModel()
                                    {
                                        operatorId = selected_operator.Id,

                                        technologyId = selected_technology.Id,

                                        data = ((double)reader.GetDouble(reader.GetOrdinal("response"))
                                               / (double)reader.GetDouble(reader.GetOrdinal("request"))),

                                        weight =
                                            reader.GetDouble(reader.GetOrdinal("request")),

                                        accurance_date =
                                            reader.GetDateTime(reader.GetOrdinal("accurance_date"))
                                    }
                                );
                            }
                        }
                    }
                }
            }

            return result;
        }

        public List<Gauge_Result_ViewModel> getUMTS_TCH_ASR(Operator selected_operator
            , Technology selected_technology, DateTime startDateTime, DateTime endDateTime)
        {
            List<Gauge_Result_ViewModel> result = new List<Gauge_Result_ViewModel>();

            string connectionString = generateConnectionString(selected_operator);

            string cmd = String.Format("select accurance_date,sum(RANAP_RABAssignment_Request ) " +
                                       " as request, sum(RANAP_RABAssignment_Response) " +
                                       " as response from (SELECT CONVERT(date, DateTime) " +
                                       " as accurance_date, RANAP_RABAssignment_Request , CASE " +
                                       " WHEN RANAP_RABAssignment_Response is not NULL THEN " +
                                       " RANAP_RABAssignment_Response " +
                                       " WHEN RANAP_RABAssignment_Response is NULL then 0 " +
                                       " END as RANAP_RABAssignment_Response" +
                                       " FROM [{0}].[dbo].[{1}]  where RANAP_RABAssignment_Request  " +
                                       " is not null and RANAP_RABAssignment_Request > 0" +
                                       " and DateTime >= '{2}' and DateTime <= '{3}' ) as result " +
                                       " group by accurance_date"
                , selected_operator.Database_Name, selected_technology.Table_Name
                , startDateTime, endDateTime);

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                if (connection.State == ConnectionState.Open)
                {
                    SqlCommand sqlCommand = new SqlCommand(cmd, connection);

                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                result.Add(
                                    new Gauge_Result_ViewModel()
                                    {

                                        operatorId = selected_operator.Id,

                                        technologyId = selected_technology.Id,

                                        data = ((double)reader.GetDouble(reader.GetOrdinal("response"))
                                                / (double)reader.GetDouble(reader.GetOrdinal("request"))),

                                        weight =
                                            reader.GetDouble(reader.GetOrdinal("request")),

                                        accurance_date =
                                            reader.GetDateTime(reader.GetOrdinal("accurance_date"))
                                    }
                                );
                            }
                        }
                    }
                }
            }

            return result;
        }
        #endregion
    }
}
